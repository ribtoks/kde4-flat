# KDE4 Flat
This is a compilation of modified themes and configurations to make your KDE4 desktop look more flat and clean. It looks a bit like new Breeze Plasma theme in KDE5, but imitating other themes it not the goal of this compilation. Сonsists of Plasma, KDM, Aurorae themes, QtCurve and GTK3 styles with KDE color scheme and wallpaper.

## Themes
 - Plasma: AtolmFlat (modified Atolm)
 - KWin: EvolvereLightPureCircleInv (from Evolvere Suite)
 - KDM: plasma5
 - KSplash: Plasma5 
 - Wallpaper: Plasma5
 - QtCurve: KNumix Evolvere (modified Knumix Light)
 - GTK3: Numix Evolvere (modified Numix-Blue)
 - Color scheme: EvolvereLight

 Use this with [Emerald](http://vinceliuice.deviantart.com/art/Emerald-icons-theme-490755152) icon theme to obtain the complete look-and-feel. 

## Dependencies
 - QtCurve
 - kcm-gtk

## Installation
This is an exaple of installation to ~/.kde and /usr/share/kde4 configuration directories. If your directory names differ, change them and create all necessary subdirectories before executing commands below:
```sh
cp -r Plasma/* ~/.kde/share/themes
sudo cp -r KDM/* /usr/share/kde4/apps/kdm/themes
cp -r KSplash/* ~/.kde/share/apps/ksplash/Themes
cp -r KWin/* .kde/share/apps/aurorae/themes
cp -r Wallpaper/* ~/.kde/share/wallpapers
cp -r GTK3/* ~/.themes
cp -r KNumix_Evolvere.qtcurve ~/.kde/share/apps/QtCurve
cp -r EvolvereLight.colors ~/.kde/share/apps/color-schemes
```
Now select this themes in KDE system settings. Do not forget to choose *Numix Evolvere* for GTK3 theme and *QtCurve* for GTK2.

## Known side-effects
KSplash theme is provided with background image sized 1920x1080 px. If your screen aspect ratio is not 16:9 (FullHD), just replace images/background.png with resized one, otherwise it will not scale automatically. Patches are welcomed.

## References to original themes
   Atolm: <http://half-left.deviantart.com/art/KDE4-Atolm-359877697>  
   Evolvere Suite: <https://github.com/franksouza183/EvolvereSuit>  
   Plasma5 KDM/KSplash: <http://kde-look.org/content/show.php/Plasma+5+KDM+Theme?content=169430>  
   KNumix Light: <http://kde-look.org/content/show.php/KNumix+Light+-+Flat+Theme?content=165849>  
   Numix-Blue: <https://github.com/aceat64/Numix-Blue>